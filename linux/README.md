#LPMS-NAV Series Lib

## Usage
### 1.Install Library programs
```bash
    $ sudo dpkg -i liblpmsnav-1.x.x-Linux.deb
```
### 2.Compiling Sample programs
```bash
    $ mkdir build
    $ cd build
    $ cmake ..
    $ make
    $ sudo ./LpmsNavSimpleExample /dev/ttyUSB0
```
### 3.Compiling ROS Example programs
```bash
# Create ROS workspace.
    $ mkdir -p ~/catkin_ws/src

# Copy lpmsnav_node folder to your ROS src folder.
    $ cp -r ./lpmsnav_node ~/catkin_ws/src

# Compiling ROS example programs
    $ cd ~/catkin_ws
    $ catkin_make
    $ source ./devel/setup.bash
```

Open a new terminal window  & run roscore
```bash
    $ roscore
```
Connect `LPMS-NAV2` sensor to PC.
Now you can run lpms_nav node on your other terminal windows.

If there are no other prompts indicating that the connection has been successful, or [See Troubleshooting](#troubleshooting).
```bash
    $ rosrun lpms_nav lpms_nav_node
```

If sensor connected you can open a new terminal window.
```bash
#Show imu message.
    $ rostopic echo /imu/data
#Plot imu message.
    $ rosrun rqt_plot rqt_plot
```

## Troubleshooting

* If prompted with the following information：
```bash
    $ rosrun lpms_nav lpms_nav_node
    #Error connecting to sensor
```
this error might be due to the fact that the current user does not have sufficient permission to access the device. To allow access to sensors connected via USB, you need to ensure that the user running the ROS sensor node has access to the /dev/ttyUSB devices. You can do this by adding the user to the dialout group. After this call, you should logout and login with this user to ensure the changed permissions are in effect.

```bash
    $ sudo adduser <username> dialout
```


## ROS Package Summary

### 1. Supported Hardware
This driver interfaces with LPMS-NAV IMU sensor from LP-Research Inc.


### 2.1 lpms_nav_node
lpms_nav_node is a driver for the LPMS-NAV Inertial Measurement Unit. It publishes  angular velocity Z-axis and acceleration data (covariances are not yet supported), and complies with the [Sensor message](https://wiki.ros.org/sensor_msgs) for [IMU API](http://docs.ros.org/api/sensor_msgs/html/msg/Imu.html)

#### 2.1.1 Published Topics
/imu/data ([sensor_msgs/Imu](http://docs.ros.org/api/sensor_msgs/html/msg/Imu.html)) 
:   Inertial data from the IMU.

/imu/rpy ([geometry_msgs/Vector3](http://docs.ros.org/api/geometry_msgs/html/msg/Vector3.html)) 
:   Euler data from the IMU.


#### 2.1.2 Services
/imu/calibrate_gyroscope ([std_srvs/Empty](http://docs.ros.org/api/std_srvs/html/srv/Empty.html)) 
:   This service activates the IMU internal gyro bias estimation function. Please make sure the IMU sensor is placed on a stable platform with minimal vibrations before calling the service. Please make sure the sensor is stationary for at least 4 seconds. The service call returns a success response once the calibration procedure is completed.

/imu/reset_heading ([std_srvs/Empty](http://docs.ros.org/api/std_srvs/html/srv/Empty.html)) 
:   This service will reset the heading (yaw) angle of the sensor to zero. 

/imu/send_getSensorData ([std_srvs/Empty](http://docs.ros.org/api/std_srvs/html/srv/Empty.html)) 
:   This service will send get_sensor_data command to sensor, and sensor will return to sensor data.
    [This applies to RS485 communication, because RS485 communication is half-duplex.]


#### 2.1.3 Parameters

~port (string, default: /dev/ttyUSB0) 
:   The port the IMU is connected to.

~baudrate (int, default NAV: 115200)
:   Baudrate for the IMU sensor.

~sensorType(int, default RS232)
：  Select sensor communication type, default is RS232.
```
Valid Parametes:
0： LPMS_NAV2_RS232 
1： LPMS_NAV2_RS485
```

~data_process_rate (int, default: 100) 
:   Data processing rate of the internal loop. This rate has to be equal or larger than the data streaming frequency of the sensor to prevent internal data queue overflow.

For further information on `LP-Research`, please visit our website:

* http://www.lp-research.com

* http://www.alubi.cn
