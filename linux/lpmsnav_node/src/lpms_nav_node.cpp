
#include <string>

#include "ros/ros.h"
#include "sensor_msgs/Imu.h"
#include "std_srvs/SetBool.h"
#include "std_srvs/Trigger.h"
#include "std_msgs/Bool.h"
#include <tf/transform_datatypes.h>

#include "lpsensor/LpmsNAVI.h"
#include "lpsensor/SensorData.h"

//! Manages connection with the sensor, publishes data
/*!
  \TODO: Make noncopyable!
 */
class LpNavProxy
{
 public:
     // Node handler
    ros::NodeHandle nh, private_nh;
    ros::Timer updateTimer;

    // Publisher
    ros::Publisher imu_pub;
    ros::Publisher rpy_publisher;

    // Service
    ros::ServiceServer gyrocalibration_serv;
    ros::ServiceServer resetHeading_serv;
    ros::ServiceServer sendGetSensorDataCommand_serv;

    sensor_msgs::Imu imu_msg;
    geometry_msgs::Vector3 rpy;

    // Parameters
    std::string comportNo;
    int baudrate;
    std::string frame_id;
    int rate;
    double timeout_threshold;
    int type;

    LpNavProxy(ros::NodeHandle h) : 
        nh(h),
        private_nh("~")
    {
        // Get node parameters
        private_nh.param<std::string>("port", comportNo, "/dev/ttyUSB0");
        private_nh.param("baudrate", baudrate, 115200);
        private_nh.param<std::string>("frame_id", frame_id, "imu");
        private_nh.param("rate", rate, 100);
        private_nh.param("timeout_threshold", timeout_threshold, 5.0);

        // Create LpmsNAV object 
        sensor1 = LpmsNAVFactory(comportNo, baudrate);

        //  Select sensor communication type, default is RS232.
        //  #define LPMS_NAV2_RS232                     0
        //  #define LPMS_NAV2_RS485                     1
        type = LPMS_NAV2_RS232;
        sensor1->setSensorType(type);

        // Set timeout threshold (us, default = 5000000)
        sensor1->setTimeoutThreshold(timeout_threshold * 1000000);

        imu_pub = nh.advertise<sensor_msgs::Imu>("data",1);
        rpy_publisher = nh.advertise<geometry_msgs::Vector3>("rpy",1);

        gyrocalibration_serv = nh.advertiseService("calibrate_gyroscope", &LpNavProxy::calibrateGyroscope, this);
        resetHeading_serv = nh.advertiseService("reset_heading", &LpNavProxy::resetHeading, this);
        sendGetSensorDataCommand_serv = nh.advertiseService("send_getSensorData", &LpNavProxy::sendGetSensorData, this);

    }

    ~LpNavProxy(void)
    {
        sensor1->disconnect();
        sensor1->release();
    }

    void update(const ros::TimerEvent& te)
    {
        LpmsNAVData sd;

        if (type == LPMS_NAV2_RS485)
        {
            sensor1->sendGetSensorDataCommand();
        }
        while (sensor1->isConnected() || sensor1->getAutoReconnect())
        {
            if (sensor1->hasData())
            {
                // Get latest data
                while (sensor1->hasData())
                {
                    sensor1->getSensorData(sd);
                }

                /* Fill the IMU message */

                // Fill the header
                imu_msg.header.stamp = ros::Time::now();
                imu_msg.header.frame_id = frame_id;

                // Fill orientation quaternion
                double yaw = sd.gAngle*3.1415926/180;

                tf::Vector3 axis(0, 0, 1);
                tf::Quaternion q(axis, yaw);
                imu_msg.orientation.w = q.w();
                imu_msg.orientation.x = q.x();
                imu_msg.orientation.y = q.y();
                imu_msg.orientation.z = q.z();

                // Fill angular velocity data
                // - scale from deg/s to rad/s
                imu_msg.angular_velocity.x = 0;
                imu_msg.angular_velocity.y = 0;
                imu_msg.angular_velocity.z = sd.gRate*3.1415926/180;

                // Fill linear acceleration data
                imu_msg.linear_acceleration.x = sd.acc[0]*9.81;
                imu_msg.linear_acceleration.y = sd.acc[1]*9.81;
                imu_msg.linear_acceleration.z = sd.acc[2]*9.81;

                //Fill roll pitch yaw  //rad
                rpy.x = 0;
                rpy.y = 0;
                rpy.z = yaw;

                // Publish the messages
                imu_pub.publish(imu_msg);
                rpy_publisher.publish(rpy);

                if (type == LPMS_NAV2_RS485)
                {
                    sensor1->sendGetSensorDataCommand();
                }

            }
        }
    }

    bool connect()
    {
        for (int i = 0; i < 3; i++)
        {
            // Connects to sensor
            if (sensor1->connect())
            {   
                return true;
            }

        }

        return false;
    }

    void run(void)
    {
        // The timer ensures periodic data publishing
        updateTimer = ros::Timer(nh.createTimer(ros::Duration(0.1/rate),
                                                &LpNavProxy::update,
                                                this));
    }

    
    ///////////////////////////////////////////////////
    // Service Callbacks
    ///////////////////////////////////////////////////
    // reset heading
    bool resetHeading (std_srvs::Trigger::Request &req, std_srvs::Trigger::Response &res)
    {
        ROS_INFO("reset_heading");
        
        // Send command
        if(sensor1->resetHeading())
        {
            res.success = true;
            res.message = "[Success] Heading reset";
            return true;
        }
        else
        {
            return false;
        }

    }


    bool calibrateGyroscope (std_srvs::Trigger::Request &req, std_srvs::Trigger::Response &res)
    {
        ROS_INFO("calibrate_gyroscope: Please make sure the sensor is stationary for 4 seconds");

        if(sensor1->resetGyroStaticBias())
        {
            ros::Duration(4).sleep();
            res.success = true;
            res.message = "[Success] Gyroscope calibration procedure completed";
            ROS_INFO("calibrate_gyroscope: Gyroscope calibration procedure completed");
            return true;
        }
        else
        {
            return false;
        }

    }

    bool sendGetSensorData (std_srvs::Trigger::Request &req, std_srvs::Trigger::Response &res)
    {
	    ROS_INFO("updating sensor data.");
        if(sensor1->sendGetSensorDataCommand())
        {
            res.success = true;
            res.message = "[Success] Command sent";
            ROS_INFO("sensor data updated");
            return true;
        }
        else
        {
            return false;
        }

    }


 private:

    // Access to LPMS data
    LpmsNAVI* sensor1;
};

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "lpms_nav_node");
    ros::NodeHandle nh("imu");

    ros::AsyncSpinner spinner(0);
    spinner.start();

    LpNavProxy lpNav(nh);
    if (!lpNav.connect())
    {
        ROS_ERROR("Error connecting to sensor");
        return 1;
    }

    lpNav.run();
    ros::waitForShutdown();

    return 0;
}