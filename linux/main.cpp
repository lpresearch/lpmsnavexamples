#include <string>
#include <thread>
#include <iostream>

#include <cstdarg>

#include "lpsensor/LpmsNAVI.h"
#include "lpsensor/SensorData.h"

using namespace std;
string TAG("Main");
LpmsNAVI* sensor1;
int g_sensorType;

bool connect()
{
    for (int i = 0; i < 3; i++)
    {
        // Connects to sensor
        if (sensor1->connect())
        {   
            return true;
        }

    }
    return false;
}

void logd(std::string tag, const char* str, ...)
{
    va_list a_list;
    va_start(a_list, str);
    if (!tag.empty())
        printf("[%s] ", tag.c_str());
    vprintf(str, a_list);
    va_end(a_list);
}

void printTask()
{
    LpmsNAVData sd;
	
    if (g_sensorType == LPMS_NAV2_RS485)
    	sensor1->sendGetSensorDataCommand();

    while (sensor1->isConnected() || sensor1->getAutoReconnect())
    {
        if (sensor1->hasData())
        {
            // get and print latest sensor data
            while (sensor1->hasData())
                sensor1->getSensorData(sd);

            logd(TAG, "t:%.2f angle: %.2f rate: %.2f Hz: %.2f\t\r", sd.timestamp, sd.gAngle, sd.gRate, sensor1->getDataFrequency());

            if (g_sensorType == LPMS_NAV2_RS485)
                    sensor1->sendGetSensorDataCommand();
            else
                this_thread::sleep_for(chrono::milliseconds(1));
        }
    }
    logd(TAG, "Thread terminated\n");
}

void printMenu()
{
    cout << "Main Menu" << endl;
    cout << "===================" << endl;
    cout << "[h] Reset sensor heading" << endl;
    cout << "[b] Calibrate gyro static bias" << endl;
    cout << "[s] Send get SensorData command" << endl;
    cout << "[q] quit" << endl;
    cout << endl;
}

int main(int argc, char** argv)
{
    if (argc != 2)
    {
        printf("Error command. Usage: CMD <COMPORT>\n");
        exit(0);
    }

    string comportNo(argv[1]);
    int baudrate = 115200;

    // Create LpmsNAV object with corresponding comport and baudrate
    sensor1 = LpmsNAVFactory(comportNo, baudrate);
    g_sensorType = LPMS_NAV2_RS232;
    sensor1->setSensorType(g_sensorType);
    
    // Cancel auto reconnection
    //sensor1->setAutoReconnect(false);

    // Set timeout threshold (us, default = 5000000)
    //sensor1->setTimeoutThreshold(1000000);

    // Connects to sensor
    if (!connect())
    {
        logd(TAG, "Error connecting to sensor\n");
        sensor1->release();
        return 0;
    }

    printMenu();
    
    // Print sensor info
    LpmsNAVInfo sensorInfo = sensor1->getSensorInfo();
    logd(TAG, "hardware version: %s\n", sensorInfo.hardwareVersion.c_str());
    logd(TAG, "firmware version: %s\n", sensorInfo.firmwareVersion.c_str());
    logd(TAG, "serialNumber: %s\n", sensorInfo.serialNumber.c_str());
    logd(TAG, "Stream Freq: %d\n", sensorInfo.streamFreq);
    logd(TAG, "Baudrate: %d\n", sensorInfo.baudrate);
    logd(TAG, "Output range: %d\n", sensorInfo.outputRange);

    // Start print data thread
    std::thread printThread(printTask);
    bool quit = false;
    while (!quit)
    {
        char cmd;
        cin >> cmd;
        switch (cmd)
        {
        case 'h':
            // reset sensor heading
            sensor1->resetHeading();
            break;

        case 'b':
            // recalibrate gyro
            sensor1->resetGyroStaticBias();
            break;

        case 's':
            //send get sensor command
            sensor1->sendGetSensorDataCommand();
            break;

        case 'q':
            // disconnect sensor
            sensor1->disconnect();
            quit = true;
            break;

        default:
            break;
        }
        this_thread::sleep_for(chrono::milliseconds(100));    
    }

    printThread.join();

    // release sensor resources
    sensor1->release();
    logd(TAG, "Bye\n");
}
