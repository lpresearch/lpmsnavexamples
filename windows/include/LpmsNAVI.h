#ifndef LPMS_NAV_I_H
#define LPMS_NAV_I_H

#ifdef _WIN32
#include "windows.h"
#endif
#include "SensorData.h"


#define SENSOR_CONFIG_UART_BAUDRATE_4800            (uint32_t)4800
#define SENSOR_CONFIG_UART_BAUDRATE_9600            (uint32_t)9600
#define SENSOR_CONFIG_UART_BAUDRATE_19200           (uint32_t)19200
#define SENSOR_CONFIG_UART_BAUDRATE_28800           (uint32_t)28800
#define SENSOR_CONFIG_UART_BAUDRATE_38400           (uint32_t)38400
#define SENSOR_CONFIG_UART_BAUDRATE_57600           (uint32_t)57600
#define SENSOR_CONFIG_UART_BAUDRATE_115200          (uint32_t)115200

#define SENSOR_CONFIG_OUTPUT_RATE_10HZ              (uint32_t)10
#define SENSOR_CONFIG_OUTPUT_RATE_25HZ              (uint32_t)25
#define SENSOR_CONFIG_OUTPUT_RATE_50HZ              (uint32_t)50
#define SENSOR_CONFIG_OUTPUT_RATE_100HZ             (uint32_t)100

#define SENSOR_CONFIG_OUTPUT_RANGE_180              180
#define SENSOR_CONFIG_OUTPUT_RANGE_360              360

class LpmsNAVI
{
public:
    ~LpmsNAVI(void) {};
    virtual void release() = 0;

    /**
    * Set sensor connection baudrate
    */
    virtual void setPCBaudrate(int baud) = 0;

#ifdef _WIN32
    /**
    * Set sensor connection COM Port
    * @param: eg. 10 for COM10
    */
    virtual void setPCPort(int port) = 0;
#else
    virtual void setPCPort(std::string port) = 0;
#endif
    
    /**
    * Initiate sensor connection
    * return true on success, false for unsuccessful connection
    */
    virtual bool connect(void) = 0;

    /**
    * Disconnect sensor
    * return true on success, false for unsuccessful connection
    */
    virtual bool disconnect(void) = 0;

    /**
    * Check sensor connection
    */
    virtual bool isConnected(void) = 0;

    /**
    * Get sensor info
    */
    virtual LpmsNAVInfo getSensorInfo(void) = 0;

    /**
    * Reset sensor heading angle
    */
    virtual bool resetHeading(void) = 0;

    /**
    * Recalibrate sensor gyro static bias
    */
    virtual bool resetGyroStaticBias(void) = 0;

    /**
    * Get current data streaming frequency
    */
    virtual float getDataFrequency(void) = 0;

    /**
    * Set sensor baudrate
    * @param: SENSOR_CONFIG_UART_BAUDRATE_X 
    */
    virtual bool setBaudrate(int baud) = 0;
    /**
    * Get current baudrate settings
    */
    virtual int getBaudrate(void) = 0;
    /**
    * Set sensor output range
    * @param: SENSOR_CONFIG_OUTPUT_RANGE_X 
    */
    virtual bool setOutputRange(int range) = 0;
    /**
    * Get current output range settings
    */
    virtual int getOutputRange(void) = 0;

    /**
    * Set sensor streaming frequency
    * @param: SENSOR_CONFIG_OUTPUT_RATE_X 
    */
    virtual bool setStreamFrequency(int freq) = 0;

    /**
    * Get current streaming frequency settings
    */
    virtual int getStreamFrequency(void) = 0;


    /**
    * Enable sensor LED
    */
    virtual bool enableLed(void) = 0;

    /**
    * Disable sensor LED
    */
    virtual bool disableLed(void) = 0;


    /**
    * Check if data available in sensor data queue
    * return numbe of data in sensor data queue
    */
    virtual int hasData(void) = 0;

    /**
    * Get data in front of sensor data queue
    * return true is data queue size is > 0
    * return false if not data available in data queue
    */
    virtual bool getSensorData(LpmsNAVData &sd) = 0;

    /**
    * Set internal data queue size (default to 10)
    */
    virtual void setSensorDataQueueSize(unsigned int size) = 0;

    /**
    * Get current data queue size (default: 10)
    */
    virtual int getSensorDataQueueSize(void) = 0;

    /**
    * Enable data sensor simulation mode
    */
    virtual void enableSimulation(void) = 0;

};

#ifdef _WIN32
    #ifdef DLL_EXPORT
        #define LPMS_NAV_API __declspec(dllexport)
    #else
        #define LPMS_NAV_API __declspec(dllimport)
    #endif

    extern "C" LPMS_NAV_API	LpmsNAVI* APIENTRY LpmsNAVFactory(int portno, int baudrate);

#else
    #define LPMS_NAV_API
    extern "C" LPMS_NAV_API LpmsNAVI* LpmsNAVFactory(std::string portno, int baudrate);
#endif


#endif