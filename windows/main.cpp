#include <string>
#include <thread>
#include <iostream>
#include <sstream>
#include "LpmsNAVI.h"
#include "SensorData.h"

using namespace std;
string TAG("Main");
LpmsNAVI* sensor1;

void logd(std::string tag, const char* str, ...)
{
    va_list a_list;
    va_start(a_list, str);
    if (!tag.empty())
        printf("[%s] ", tag.c_str());
    vprintf(str, a_list);
    va_end(a_list);
}

void printTask()
{
    LpmsNAVData sd;

    while (sensor1->isConnected())
    {
        // get and print sensor data
        if (sensor1->getSensorData(sd))
        {
            logd(TAG, "t:%.2f angle: %.2f rate: %.2f Hz: %.2f\t\r", sd.timestamp, sd.gAngle, sd.gRate, sensor1->getDataFrequency());
        }
        this_thread::sleep_for(chrono::milliseconds(1));
    }
    logd(TAG, "Thread terminated\n");
}

void printMenu()
{
    cout << "Main Menu" << endl;
    cout << "===================" << endl;
    cout << "[h] Reset sensor heading" << endl;
    cout << "[b] Calibrate gyro static bias" << endl;
    cout << "[q] quit" << endl;
    cout << endl;
}

int main(int argc, char** argv)
{
    if (argc != 2)
    {
        printf("Error command. Usage: CMD <COMPORT>\n");
        exit(0);
    }

    std::istringstream ss(argv[1]);

    int comportNo = 0;
    if (!(ss >> comportNo)) 
    {
        printf("Invalid comport: %s\n", argv[1]);
        exit(0);
    }
    int baudrate = 115200;

    // Create LpmsNAV object with corresponding comport and baudrate
    sensor1 = LpmsNAVFactory(comportNo, baudrate);

    // Enable simulation mode
    //sensor1->enableSimulation();

    // Connects to sensor
    if (!sensor1->connect())
    {
        logd(TAG, "Error connecting to sensor\n");
        sensor1->release();
        return 0;
    }

    printMenu();
    
    // Print sensor info
    LpmsNAVInfo sensorInfo = sensor1->getSensorInfo();
    logd(TAG, "hardware version: %s\n", sensorInfo.hardwareVersion.c_str());
    logd(TAG, "firmware version: %s\n", sensorInfo.firmwareVersion.c_str());
    logd(TAG, "serialNumber: %s\n", sensorInfo.serialNumber.c_str());
    logd(TAG, "Stream Freq: %d\n", sensorInfo.streamFreq);
    logd(TAG, "Baudrate: %d\n", sensorInfo.baudrate);
    logd(TAG, "Output range: %d\n", sensorInfo.outputRange);

    // Start print data thread
    std::thread printThread(printTask);
    bool quit = false;
    while (!quit)
    {
        char cmd;
        cin >> cmd;
        switch (cmd)
        {
        case 'h':
            // reset sensor heading
            sensor1->resetHeading();
            break;

        case 'b':
            // recalibrate gyro
            sensor1->resetGyroStaticBias();
            break;

        case 'q':
            // disconnect sensor
            sensor1->disconnect();
            quit = true;
            break;

        default:
            break;
        }
        this_thread::sleep_for(chrono::milliseconds(100));    
    }

    printThread.join();

    // release sensor resources
    sensor1->release();
    logd(TAG, "Bye\n");
}